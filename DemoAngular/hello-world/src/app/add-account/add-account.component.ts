import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Account } from '../account';
import { NgserviceService } from '../ngservice.service';

@Component({
  selector: 'app-add-account',
  templateUrl: './add-account.component.html',
  styleUrls: ['./add-account.component.css']
})
export class AddAccountComponent implements OnInit {
  account = new Account();

  constructor(private _service:NgserviceService) { }

  ngOnInit(): void {
  }

  addAccountFormSubmit(){
    this._service.addNewAccount(this.account).subscribe(
      data=>console.log("data added successfully"),
      error=>console.log("error occured")
    )
  }

}
