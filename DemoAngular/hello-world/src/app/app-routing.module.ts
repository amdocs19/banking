import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AddAccountComponent } from './add-account/add-account.component';
import { UserListComponent } from './user-list/user-list.component';
import { EditUserDetailsComponent } from './edit-user-details/edit-user-details.component';
import { AccountListComponent } from './account-list/account-list.component';

const routes: Routes = [
  {path:'', component: HomeComponent},
  {path:'login', component: SignInComponent},
  {path:'register',component: SignUpComponent},
  {path:'addAccount',component: AddAccountComponent},
  {path:'getUsers',component: UserListComponent},
  {path:'editUser',component: EditUserDetailsComponent},
  {path:'editUser/:id',component: EditUserDetailsComponent},
  {path:'viewAccount/:id',component: AccountListComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
