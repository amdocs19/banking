import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Account } from '../account';
import { NgserviceService } from '../ngservice.service';

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css']
})
export class AccountListComponent implements OnInit {
  //acc= new Account();
  accountData:any=[];
  constructor(private _route:Router,private _service:NgserviceService,private _activatedRoute:ActivatedRoute) { }

  selectId: any;

  ngOnInit(): void {
      // this._activatedRoute.paramMap.subscribe(
      // params=>{
      //     this.selectId=parseInt(params.get('id'));
      // }  
      // )
      this._activatedRoute.paramMap.subscribe(
        params=>{
          this.selectId=(params.get('id'));
        }
       
      )
      this._service.getAccount(this.selectId).subscribe(
        data=>{
          console.log("Data received");
          this.accountData=data;
        },
        error=>{
          console.log("exception occured");
        }
      )
      console.log(this.selectId);
  }

}
