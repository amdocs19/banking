import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { User } from './user';
import { Account } from './account';

@Injectable({
  providedIn: 'root'
})
export class NgserviceService {

  constructor(private _http:HttpClient) { }

  fetchUserList():Observable<any>{
    return this._http.get<any>("http://localhost:8080/allUsers");
  }

  addNewUser(user:User):Observable<any>{
    return this._http.post<any>("http://localhost:8080/addUser",user);
  }

  addNewAccount(account:Account):Observable<any>{
    return this._http.post<any>("http://localhost:8080/addAccount",account);
  }
  
  editUserDetails(user:User):Observable<any>{
    return this._http.put<any>("http://localhost:8080/updateUser",user);
  }

  deletUserDetails(id:number):Observable<any>{
    return this._http.delete<any>("http://localhost:8080/deleteUser/"+id);
  }

  fetchUserByID(id:number):Observable<any>{
    return this._http.get<any>("http://localhost:8080/getUserByID/"+id);
  }

  getAccount(id:number):Observable<any>{
    return this._http.get<any>("http://localhost:8080/getbyuserid/"+id);
  }
}
