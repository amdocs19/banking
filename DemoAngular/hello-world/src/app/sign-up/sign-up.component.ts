import { Component, OnInit } from '@angular/core';
import { NgserviceService } from '../ngservice.service';
import { NgForm } from '@angular/forms';
import { User } from '../user';



@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  user = new User();

  constructor(private _service:NgserviceService) { }

  ngOnInit(): void {
  }

  addUserFormSubmit(addUserForm:any){
    this._service.addNewUser(this.user).subscribe(
      data=>console.log("data added successfully"),
      error=>console.log("error occured")
    )
    console.log(addUserForm);
  }



}
