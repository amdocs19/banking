import { Component, OnInit } from '@angular/core';
import { NgserviceService } from '../ngservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  userData:any=[];

  constructor(private router: Router,private _service: NgserviceService) {
    let userData = this._service.fetchUserList().subscribe(data=>{
     this.userData=data;
    });
    console.log(userData);
    
   }

  ngOnInit(){
    this._service.fetchUserList().subscribe(
      data=>console.log("response received"),
      error=>console.log("exception occured")
    )
    
  }

  btnClickEdit=() =>{
    this.router.navigate(['/editUser']);
  }

  goToEditUser(id:number){
    console.log(id);
    this.router.navigate(['/editUser',id]);
  }

  goToDeleteUser(id:number){
    this._service.deletUserDetails(id).subscribe(
      data=>console.log("response received"),
      error=>console.log("exception occured")
    )
    window.location.reload();
  }

  // btnDeleteClick(){
   
  //     this._service.deleteUser(this.user).subscribe(
  //       data=>console.log("data added successfully"),
  //       error=>console.log("error occured")
  //     )
 
  
  // }

  goToViewAccount(id:number){
    console.log(id);
    this.router.navigate(['/viewAccount',id]);
  }
    //this.router.navigate(['/getUsers']);
  }


